import 'package:bmi_and_snippets/constants/app_texts_styles.dart';
import 'package:flutter/material.dart';

class CalculateButton extends StatelessWidget {
  const CalculateButton({
    super.key,
    required this.onpressed,
  });
  final void Function() onpressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onpressed,
      child: Container(
        height: 73,
        color: Colors.pink,
        child: const Center(
          child: Text(
            'CALCULATOR',
            style: AppTextStyle.calculateStyle,
          ),
        ),
      ),
    );
  }
}
