import 'package:bmi_and_snippets/constants/app_colors.dart';
import 'package:bmi_and_snippets/constants/app_texts_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StatusCard extends StatelessWidget {
  const StatusCard({
    super.key,
    required this.child,
  });
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Card(
        color: AppColors.cardColor,
        child: child,
      ),
    );
  }
}

class Gender extends StatelessWidget {
  const Gender({
    super.key,
    required this.icon,
    required this.text,
    required this.check,
  });

  final IconData icon;
  final String text;
  final bool check;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          size: 70,
          color: check ? Colors.red : Colors.white,
        ),
        Text(
          text,
          style: AppTextStyle.titleStyle,
        )
      ],
    );
  }
}

class Height extends StatelessWidget {
  const Height({
    super.key,
    required this.weight,
    required this.cm,
    required this.onchanged,
    required this.height,
  });

  final String weight;
  final String cm;
  final int height;
  final void Function(double) onchanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          'HEIGHT',
          style: AppTextStyle.titleStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.baseline,
          textBaseline: TextBaseline.alphabetic,
          children: [
            Text(
              height.toString(),
              style: AppTextStyle.boldStyle,
            ),
            Text(
              cm,
              style: AppTextStyle.titleStyle,
            ),
          ],
        ),
        SizedBox(
          width: 300,
          child: CupertinoSlider(
            value: height.toDouble(),
            min: 0,
            max: 250,
            thumbColor: AppColors.pinkColor,
            activeColor: Colors.white,
            onChanged: onchanged,
          ),
        )
      ],
    );
  }
}

class AgeWeight extends StatelessWidget {
  const AgeWeight({
    super.key,
    required this.text,
    required this.content,
    required this.addPressed,
    required this.remPressed,
  });

  final String text;
  final int content;
  final void Function() addPressed;
  final void Function() remPressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          text,
          style: AppTextStyle.titleStyle,
        ),
        Text(
          content.toString(),
          style: AppTextStyle.boldStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 45,
              height: 45,
              child: FloatingActionButton(
                backgroundColor: AppColors.greyColor,
                onPressed: remPressed,
                child: const Icon(
                  Icons.remove,
                  size: 45,
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            SizedBox(
              width: 45,
              height: 45,
              child: FloatingActionButton(
                backgroundColor: AppColors.greyColor,
                onPressed: addPressed,
                child: const Icon(
                  Icons.add,
                  size: 45,
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
