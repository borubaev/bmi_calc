import 'package:flutter/material.dart';

class AppTexts {
  static const bmi = 'BMI CALCULATOR';
  static const male = Text('Male');
  static const female = Text('Female');
  static const height = Text('HEIGHT');
  static const cm = Text('cm');
  static const weight = Text('WEIGHT');
  static const age = Text('Age');
}
