import 'package:flutter/material.dart';

class AppTextStyle {
  static const titleStyle =
      TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white);
  static const boldStyle =
      TextStyle(fontSize: 40, fontWeight: FontWeight.w800, color: Colors.white);
  static const calculateStyle =
      TextStyle(fontSize: 22, fontWeight: FontWeight.w500, color: Colors.white);
}
