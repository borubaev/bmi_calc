// ignore_for_file: file_names

import 'dart:math';

import 'package:bmi_and_snippets/constants/app_colors.dart';
import 'package:bmi_and_snippets/constants/app_texts.dart';
import 'package:bmi_and_snippets/constants/app_texts_styles.dart';
import 'package:flutter/material.dart';
import '../components/CalculateButton.dart';
import '../components/StatusCard.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int age = 18;
  int weight = 50;
  bool isTrue = false;
  double height = 180;

  void resultat() {
    final finish = weight / pow(height / 100, 2);
    if (finish <= 18.5) {
      _dialogBuilder(context, 'Сиз арыксыз');
    } else if (finish <= 25) {
      _dialogBuilder(context, 'Сиздин салмагыныз нормалдуу');
    } else if (finish <= 30) {
      _dialogBuilder(context, 'Сизде ашыкча салмактуулук бар');
    } else if (finish > 30) {
      _dialogBuilder(
          context, 'Сиздин салмагыныз ден-соолугунузна зыян алып келет');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.bgcColor,
      appBar: AppBar(
        backgroundColor: AppColors.bgcColor,
        centerTitle: true,
        title: const Text('BMI CALCULATOR'),
        elevation: 0,
      ),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 21, top: 32, right: 21, bottom: 41),
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  StatusCard(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          isTrue = true;
                        });
                      },
                      child: Gender(
                        icon: Icons.male,
                        text: 'MALE',
                        check: isTrue,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 29,
                  ),
                  StatusCard(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          isTrue = false;
                        });
                      },
                      child: Gender(
                        icon: Icons.female,
                        text: 'FEMALE',
                        check: !isTrue,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 18,
            ),
            Expanded(
              child: Row(
                children: [
                  StatusCard(
                    child: Height(
                      weight: '$height',
                      cm: 'cm',
                      onchanged: (value) {
                        setState(() {
                          height = value;
                        });
                      },
                      height: height.toInt(),
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 19,
            ),
            Expanded(
              child: Row(
                children: [
                  StatusCard(
                    child: AgeWeight(
                      text: 'WEIGHT',
                      content: weight,
                      addPressed: () {
                        setState(() {
                          weight++;
                        });
                      },
                      remPressed: () {
                        setState(() {
                          weight--;
                        });
                      },
                    ),
                  ),
                  const SizedBox(
                    width: 29,
                  ),
                  StatusCard(
                    child: AgeWeight(
                      text: 'AGE',
                      content: age,
                      addPressed: () {
                        setState(() {
                          age++;
                        });
                      },
                      remPressed: () {
                        setState(() {
                          age--;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: CalculateButton(
        onpressed: () {
          resultat();
        },
      ),
    );
  }
}

Future<void> _dialogBuilder(BuildContext context, String text) {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        backgroundColor: AppColors.bgcColor,
        title: const Text(
          AppTexts.bmi,
          textAlign: TextAlign.center,
          style: AppTextStyle.titleStyle,
        ),
        content: Text(
          text,
          style: AppTextStyle.calculateStyle,
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          TextButton(
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.labelLarge,
            ),
            child: const Text('Chyguu'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
